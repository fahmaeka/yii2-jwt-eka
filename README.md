### Step-by-step usage API Jwt

1. Clone repository

    ```
    git clone https://fahmaeka@bitbucket.org/fahmaeka/yii2-jwt-eka.git
    ```

2. Install component

    ```
    composer install
    ```

3. Run Web Server
   Setelah instalasi selesai, konfigurasikan server web PHP bawaan dengan menjalankan perintah konsol berikut saat berada di direktori web proyek:

    ```
    php yii serve
    ```

### Test Case

1. Kirim params data login ke endpoint dengan username dan password ```admin```,

    ```
    http://localhost:8080/web?r=rest/login
    ```
    
    apabila success maka respon data dari endpoint ini berupa token Jwt.
    
2. Token tersebut selanjutnya digunakan sebagai value di Authorization nya. Untuk menampilkan daftar menu makanan bisa mengakses endpoint dibawah ini

    ```
    http://localhost:8080/web/?r=rest/menu-catering
    ```
    
    akan menampilkan daftar menu katering.
    