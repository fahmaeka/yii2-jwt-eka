<?php

namespace app\controllers;

use sizeg\jwt\Jwt;
use sizeg\jwt\JwtHttpBearerAuth;
use app\models\LoginForm;
use app\models\User;
use Yii;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;

class RestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
            'optional' => [
                'login',
            ],
        ];

        return $behaviors;
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogin()
    {
        $username = Yii::$app->request->post('username');
        $password = Yii::$app->request->post('password');

        $provider = new ActiveDataProvider([
            'query' => User::findByUsername($username),
        ]);
    
        $result = $provider->query;
        if($result){
            
            if ($password === $result->password) 
            {
                $signer = new \Lcobucci\JWT\Signer\Hmac\Sha256();
                $jwt = Yii::$app->jwt;
                $token = $jwt->getBuilder()
                    ->setIssuer('http://example.com')
                    ->setAudience('http://example.org')
                    ->setId($result->id, true)
                    ->setIssuedAt(time())
                    ->setExpiration(time() + 3600)
                    ->set('uid', 100)
                    ->sign($signer, $jwt->key)
                    ->getToken();

                return $this->asJson([
                    'status' => true,
                    'token'  => (string)$token,
                ]);
            } 
            else 
            {
                return $this->asJson([
                    'status'  => false,
                    'message' => 'Wrong username or password.'
                ]);
            }
        }
        else
        {
            return $this->asJson([
                'status'  => false,
                'message' => 'username not found.'
            ]);
        }

    }

    /**
     * @return \yii\web\Response
     */
    public function actionMenuCatering()
    {
        return $this->asJson([
            'success' => true,
            'daftar-menu' => self::$menus
        ]);
    }

    private static $menus = [
        '1' => [
            'lauk' => 'Udang Cah Jamur',
            'sayur' => 'Cah Kangkung',
            'sambal' => 'Sambal Botol',
            'kerupuk' => 'Kerupuk Udang',
            'buah' => 'Semangka',
        ],
        '2' => [
            'lauk' => 'Dendeng Daging',
            'sayur' => 'Sayur Bening',
            'sambal' => 'Sambal Terasi',
            'kerupuk' => 'Kerupuk Kanji',
            'buah' => 'Jeruk',
        ],
        '3' => [
            'lauk' => 'Tumis Udang Limau',
            'sayur' => 'Oseng-oseng Kacang Panjang',
            'sambal' => 'Sambal Bajak',
            'kerupuk' => 'Emping',
            'buah' => 'Anggur',
        ]
    ];
}